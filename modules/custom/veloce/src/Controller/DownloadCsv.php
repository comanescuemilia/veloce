<?php

namespace Drupal\veloce\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\migrate_tools\MigrateTools;
use GuzzleHttp\Client;

class DownloadCsv extends ControllerBase
{
  private $fields = [
    'field_aderenta_umed',
    'field_diametru',
    'field_eficienta_consum',
    'field_inaltime',
    'field_indice_greutate',
    'field_indice_viteza',
    'field_latime',
    'field_producator',
  ];

  public function download($csvId){
    $client = new Client();
    $response = $client->request('GET', 'https://api.2performant.com/feed/'.$csvId.'.csv');
    $text = $response->getBody()->getContents();
    $module_path = drupal_get_path('module', 'veloce');
    $my_file = fopen($module_path . '/csv/' . $csvId.'.csv', "w");
    fwrite($my_file, $text);
    fclose($my_file);
    $this->removeValues($this->fields);
    return array(
      '#type' => 'markup',
      '#markup' => $this->t('CSV @csvId.csv has been downloaded and list terms wiped',['@csvId' => $csvId]),
    );
  }

  // Reset values for fields that are type of list text.
  public function removeValues($fields)
  {
    foreach ($fields as $field) {
      $field_loaded = FieldStorageConfig::loadByName('taxonomy_term', $field);
      $allowed_values = $field_loaded->getSetting('allowed_values');
      if (!empty($allowed_values)) {
        $field_loaded->setSetting('allowed_values', [])->save();
      }
    }
  }

}
