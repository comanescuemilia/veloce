<?php
/**
 * @file
 * Contains \Drupal\mymodule\Plugin\QueueWorker\EmailQueue.
 */
namespace Drupal\veloce\Plugin\QueueWorker;

use Drupal\Core\Queue\QueueWorkerBase;
use Goutte\Client;

/**
 * Processes Tasks for Learning.
 *
 * @QueueWorker(
 *   id = "taxonomy_update",
 *   title = @Translation("Taxonomy update"),
 *   cron = {"time" = 60}
 * )
 */
class TaxonomyUpdate extends QueueWorkerBase {
  /**
   * {@inheritdoc}
   */
  public function processItem($data) {

      $link = $data->get('field_product_affiliate_link')->getValue()[0]['uri'];
      $data->set('field_product_affiliate_link', [

      'uri' => $link,

      'title' => 'Cumpără pe '. $data->get('field_advertiser_name')->getValue()[0]['value'],

      'options' => ['attributes' => [

        'target' => '_blank'

      ]]

      ])->save();
      $link = urldecode(substr($link, strpos($link, "=") + 1));
      $parametru = $this->crawler($link, '.prod-param .lc');
      $valoare = $this->crawler($link, '.prod-param .rc');

      $client = new Client();
      $request = $client->request('GET',$link);
      $producator = $request->filter('.brand')->link()->getUri();
      $producator = substr($producator, strpos($producator, "=") + 1);

      if ($producator === 'Bfgoodrich') {
        $producator = 'BFGoodrich';
      }

      $data->set('field_producator', $producator);
      // This will "check Generate automatic URL alias".
      $data->path->pathauto = 1;

      $discount = $request->filter('.prod-discount-val');
      if ($discount->count() > 0 ){
        $discount = $discount->text();
        $data->set('field_discount', preg_replace('/[^0-9]/', '', $discount));
      }
      elseif ($discount->count() === 0 && !empty($data->get('field_discount')->getValue()[0]['value'])){
        $data->set('field_discount', 0);
      }

      $parametrii = array_filter(array_combine($parametru, $valoare));

      if (key_exists('Sezon', $parametrii)) {
        $data->set('field_sezon', $parametrii['Sezon']);
      }
      if (key_exists('Latime', $parametrii)) {
        $data->set('field_latime', $parametrii['Latime']);
      }
      if (key_exists('Inaltime', $parametrii)) {
        $data->set('field_inaltime', $parametrii['Inaltime']);
      }
      if (key_exists('Diametru (inch)', $parametrii)) {
        $data->set('field_diametru', $parametrii['Diametru (inch)']);
      }
      if (key_exists('Indice viteza', $parametrii)) {
        $data->set('field_indice_viteza', $parametrii['Indice viteza']);
      }
      if (key_exists('Indice greutate', $parametrii)) {
        $data->set('field_indice_greutate', $parametrii['Indice greutate']);
      }
      if (key_exists('Tip cargo', $parametrii)) {
        $parametrii['Tip cargo'] === 'DA' ? $parametrii['Tip cargo'] = 1 : $parametrii['Tip cargo'] = 0;
        $data->set('field_tip_cargo', $parametrii['Tip cargo']);
      }
      if (key_exists('Ramforsat', $parametrii)) {
        $parametrii['Ramforsat'] === 'DA' ? $parametrii['Ramforsat'] = 1 : $parametrii['Ramforsat'] = 0;
        $data->set('field_ranforsat', $parametrii['Ramforsat']);
      }
      if (key_exists('Eficienta consum', $parametrii)) {
        $data->set('field_eficienta_consum', $parametrii['Eficienta consum']);
      }
      if (key_exists('Aderenta umed', $parametrii)) {
        $data->set('field_aderenta_umed', $parametrii['Aderenta umed']);
      }
      if (key_exists('Nivel zgomot (db)', $parametrii)) {
        $data->set('field_nivel_zgomot_db_', $parametrii['Nivel zgomot (db)']);
      }
      if (key_exists('Greutate (kg)', $parametrii)) {
        $data->set('field_greutate_kg_', $parametrii['Greutate (kg)']);
      }
      if (key_exists('Runflat', $parametrii)) {
        $parametrii['Runflat'] === 'DA' ? $parametrii['Runflat'] = 1 : $parametrii['Runflat'] = 0;
        $data->set('field_runflat', $parametrii['Runflat']);
      }
      $data->save();
    }

  public function crawler($uri, $htmlfilter){
    $client = new Client();
    $request = $client->request('GET',$uri);

    $link = $request->filter($htmlfilter)
      ->each(function ($node){
        return trim($node->text());
      });

    return $link;
  }

}
