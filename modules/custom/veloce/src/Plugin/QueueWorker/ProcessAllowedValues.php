<?php

namespace Drupal\veloce\Plugin\QueueWorker;

use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\field\Entity\FieldStorageConfig;
use Goutte\Client;

/**
 * Processes Allowed values for fields of type list.
 *
 * @QueueWorker(
 *   id = "process_allowed_values",
 *   title = @Translation("Update allowed values"),
 *   cron = {"time" = 60}
 * )
 */
class ProcessAllowedValues extends QueueWorkerBase
{
  /**
   * {@inheritdoc}
   */
  public function processItem($data)
  {
    $link = $data->get('field_product_affiliate_link')->getValue()[0]['uri'];
    $link = urldecode(substr($link, strpos($link, "=") + 1));
    $parametru = $this->crawler($link, '.prod-param .lc');
    $valoare = $this->crawler($link, '.prod-param .rc');

    $parametrii = array_filter(array_combine($parametru, $valoare));

    $client = new Client();
    $request = $client->request('GET', $link);
    $producator = $request->filter('.brand')->link()->getUri();
    $producator = substr($producator, strpos($producator, "=") + 1);

    if ($producator === 'Bfgoodrich') {
      $producator = 'BFGoodrich';
    }

    if (key_exists('Aderenta umed', $parametrii)) {
      $this->processField('field_aderenta_umed', $parametrii['Aderenta umed']);
    }
    if (key_exists('Diametru (inch)', $parametrii)) {
      $this->processField('field_diametru', $parametrii['Diametru (inch)']);
    }
    if (key_exists('Eficienta consum', $parametrii)) {
      $this->processField('field_eficienta_consum', $parametrii['Eficienta consum']);
    }
    if (key_exists('Inaltime', $parametrii)) {
      $this->processField('field_inaltime', $parametrii['Inaltime']);
    }
    if (key_exists('Indice greutate', $parametrii)) {
      $this->processField('field_indice_greutate', $parametrii['Indice greutate']);
    }
    if (key_exists('Indice viteza', $parametrii)) {
      $this->processField('field_indice_viteza', $parametrii['Indice viteza']);
    }
    if (key_exists('Latime', $parametrii)) {
      $this->processField('field_latime', $parametrii['Latime']);
    }
    if($producator) {
      $this->processField('field_producator', $producator);
    }
  }

  public function processField($field, $value)
  {
    $field_loaded = FieldStorageConfig::loadByName('taxonomy_term', $field);
    $allowed_values = $field_loaded->getSetting('allowed_values');

    if (!in_array($value, $allowed_values) && !empty($value)) {
      $allowed_values[$value] = $value;
    }
    asort($allowed_values);
    $field_loaded->setSetting('allowed_values', $allowed_values)->save();
  }

  public function crawler($uri, $htmlfilter){
    $client = new Client();
    $request = $client->request('GET',$uri);

    $link = $request->filter($htmlfilter)
      ->each(function ($node){
        return trim($node->text());
      });

    return $link;
  }
}
