<?php

namespace Drupal\veloce\Plugin\migrate\process;

use Drupal\field\Entity\FieldStorageConfig;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\Row;

/**
 * Will take info from title/description
 * and will put it in the entity's fields.
 *
 * @MigrateProcessPlugin(
 *   id = "split_description"
 * )
 *
 * To do custom value transformations use the following:
 *
 * @code
 * field_text:
 *   plugin: transform_value
 *   source: text
 * @endcode
 *
 */
class SplitDescription extends ProcessPluginBase
{
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    // $value example: "Hankook i*cept RS (W442) ( 155/80 R13 79T SBL )"
    // Get the last occurence of (.
    $str_pos = strrpos($value, "(");
    // Title is before the last (.
    $title = rtrim(substr($value, 0, $str_pos));
    // Description is after the last (.
    $description = substr($value, $str_pos);
    //If description contains more than a )
    // it means our description starts
    // on the first occurence of (
    // Ex "Michelin Pilot Sport Cup 2 ( 325/30 ZR20 (106Y) XL MO )"
    if(substr_count($description, ")") > 1){
      $str_pos = strpos($value, "(");
      $description = substr($value, $str_pos);
      // Title is also before the first occurence of (
      $title = rtrim(substr($value, 0, $str_pos));
    }

    // Remove any ( or ) left in description.
    $description = trim($description, "(\)");

    // Separate description values by their empty space.
    $array = explode(" ", $description);

    // This will remove the empty array elements and will reindex array.
    $final = array_values(array_filter($array));

    // Explode latime/inaltime
    if(strpos($final[0],"/")){
      $final[0] = explode('/',$final[0]);
    }

    // Scoate R si alte litere din diametru.
    if(preg_match("/[^0-9]/", $final[1])){
      $final[1] = preg_replace("/[^0-9]/", "",$final[1]);
    }

    // Remove ( ) if any from indice greutate si indice viteza.
    $final[2] = trim($final[2],"(\)");

    // Explode indice greutate si indice viteza,
    // ex "79T" becomes 79 T stored in 2 keys.
    // 107/105T becomes 107/105 T stored in 2 keys.
    if(preg_match('/(?<=[0-9])(?=[a-z]+)/i',$final[2])){
      $final[2] = preg_split('/(?<=[0-9])(?=[a-z]+)/i',$final[2]);
    }

    // Set each field with its value.
    if($title){
        $row->setDestinationProperty('name', $title);
    }
    if($final[0][0]){
      $row->setDestinationProperty('field_latime', $final[0][0]);
      // This will append value to allowed values list.
      $this->processField('field_latime', $final[0][0]);
    }
    if($final[0][1]){
      $row->setDestinationProperty('field_inaltime', $final[0][1]);
      $this->processField('field_inaltime', $final[0][1]);
    }
    if($final[1]){
      $row->setDestinationProperty('field_diametru', $final[1]);
      $this->processField('field_diametru', $final[1]);
    }

    if($final[2]){
      if(is_array($final[2])){
        // Indice viteza is the last element of $final[2].
        $indice_viteza = array_pop($final[2]);
        // If both are string final[2] is indice greutate
        // and $final[3] is indice viteza.
        // This is for cases like "102 S"
      } elseif (is_string($final[2]) && is_string($final[3])) {
        $indice_viteza = $final[3];
      }

      $row->setDestinationProperty('field_indice_viteza', $indice_viteza);
      $this->processField('field_indice_viteza', $indice_viteza);
    }

    // If we have products with 2 values for indice greutate
    // like 109/107 split them in a single value.
    if($final[2]){
      $exploded = explode('/',$final[2][0]);
      if (count($exploded) === 2) {
        $this->processField('field_indice_greutate', $exploded[0]);
        $this->processField('field_indice_greutate', $exploded[1]);
      }
      elseif (count($exploded) === 1) {
        $this->processField('field_indice_greutate', $final[2][0]);
      }
      $row->setDestinationProperty('field_indice_greutate', $final[2][0]);
    }
    if(strpos($value,'XL')){
      $row->setDestinationProperty('field_ranforsat', 1);
    }
    if(strpos($value,'RF')){
      $row->setDestinationProperty('field_runflat', 1);
    }

    // Process also field producator here
    // so we don't create another plugin.
    $producator = $row->getDestinationProperty('field_producator');
    if (!empty($producator)) {
      $this->processField('field_producator', $producator);
    }
  }

  // For fields that are type of list text append the allowed values.
  public function processField($field, $value)
  {
    $field_loaded = FieldStorageConfig::loadByName('taxonomy_term', $field);
    $allowed_values = $field_loaded->getSetting('allowed_values');

    if (!in_array($value, $allowed_values) && !empty($value)) {
      $allowed_values[$value] = $value;
    }
    asort($allowed_values);
    $field_loaded->setSetting('allowed_values', $allowed_values)->save();
  }
}
