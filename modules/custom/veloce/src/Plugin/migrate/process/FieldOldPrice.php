<?php

namespace Drupal\veloce\Plugin\migrate\process;

use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\Row;

/**
 * Perform custom value transformations.
 *
 * @MigrateProcessPlugin(
 *   id = "field_old_price"
 * )
 *
 * To do custom value transformations use the following:
 *
 * @code
 * field_text:
 *   plugin: transform_value
 *   source: text
 * @endcode
 *
 */

class FieldOldPrice extends ProcessPluginBase
{
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property)
  {
    $new_price = $row->getSourceProperty('price');
    // If old price is bigger than new price
    // we can set it, otherwise we should not put it
    // if it is smaller than new price.
    if($value > $new_price){
      $row->setDestinationProperty($destination_property, $value);
    }
  }
}
