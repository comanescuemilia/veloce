<?php

namespace Drupal\veloce\Plugin\migrate\process;

use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\Row;

/**
 * Perform custom value transformations.
 *
 * @MigrateProcessPlugin(
 *   id = "affiliate_link"
 * )
 *
 * To do custom value transformations use the following:
 *
 * @code
 * field_text:
 *   plugin: transform_value
 *   source: text
 * @endcode
 *
 */
class AffiliateLink extends ProcessPluginBase
{
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property)
  {
    // Get the advertiser website.
    $advertiser = $row->getDestinationProperty('field_advertiser_name');

    $row->setDestinationProperty('field_product_affiliate_link', [

      'uri' => $value,

      'title' => 'Cumpără pe ' . $advertiser,

      'options' => ['attributes' => [

        'target' => '_blank'

      ]]

    ]);
  }
}
