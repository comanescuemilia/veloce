<?php

namespace Drupal\veloce\Plugin\migrate\process;

use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\MigrateSkipRowException;
use Drupal\migrate\Row;

/**
 * Perform custom value transformations.
 *
 * @MigrateProcessPlugin(
 *   id = "skip_jante"
 * )
 *
 * To do custom value transformations use the following:
 *
 * @code
 * field_text:
 *   plugin: transform_value
 *   source: text
 * @endcode
 *
 */

class SkipJante extends ProcessPluginBase
{
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property){
    // Did not figured how to skip on value
    // that contains jante using default plugins so i made this.
    if (strpos($value, 'Janta') !== false ){
      throw new MigrateSkipRowException('Janta is not allowed', FALSE);
    }
    else {
      $row->setDestinationProperty($destination_property, $value);
    }
  }
}
