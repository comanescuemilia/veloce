<?php

namespace Drupal\veloce\Plugin\migrate\process;

use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\Row;

/**
 * Perform custom value transformations.
 *
 * @MigrateProcessPlugin(
 *   id = "field_new_price"
 * )
 *
 * To do custom value transformations use the following:
 *
 * @code
 * field_text:
 *   plugin: transform_value
 *   source: text
 * @endcode
 *
 */

class FieldNewPrice extends ProcessPluginBase
{
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property){
    $old_price = $row->getDestinationProperty('field_old_price');

    if (!empty($value) && !empty($old_price) && ($value < $old_price)){
      $discount = ($value - $old_price) / $old_price;
      $discount = abs(round((float)$discount * 100 ));
      $row->setDestinationProperty($destination_property, $value);
      $row->setDestinationProperty('field_discount', $discount);
    } else {
      $row->setDestinationProperty($destination_property, $value);
    }
  }

}
