<?php

namespace Drupal\veloce\Plugin\migrate\process;

use Drupal\field\Entity\FieldStorageConfig;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\Row;

/**
 * Will take info from title/description
 * and will put it in the entity's fields.
 *
 * @MigrateProcessPlugin(
 *   id = "sezon"
 * )
 *
 * To do custom value transformations use the following:
 *
 * @code
 * field_text:
 *   plugin: transform_value
 *   source: text
 * @endcode
 *
 */
class Sezon extends ProcessPluginBase
{
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property)
  {
    if($value === "Cauciucuri de vara"){
      $row->setDestinationProperty($destination_property, 'Vara');
    }elseif ($value === "Cauciucuri de iarna"){
      $row->setDestinationProperty($destination_property, 'Iarna');
    }elseif ($value === "Cauciucuri pentru toate anotimpurile"){
      $row->setDestinationProperty($destination_property, 'All Season');
    }
  }
}
